<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome</title>
</head>
<body>
    <h1>SELAMAT DATANG!</h1>
    <h2>Terima kasih telah bergabung di Sanberbook. Social Media kita bersama!</h2>

    <p>Nama depan : {{$nama_depan}}</p>
    <p>Nama belakang : {{$nama_belakang}}</p>
    <p>Jenis kelamin: {{$jk}}</p>
    <p>Bahasa : {{$bahasa}}</p>
</body>
</html>
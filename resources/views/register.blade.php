<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/kirim" method="post">
        @csrf
        <label>First name:</label><br><br>
        <input type="text" name="first_name"><br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender" value="Male" checked>Male<br>
        <input type="radio" name="gender" value="Female">Female<br>
        <input type="radio" name="gender" value="Other">Other<br><br>
        <label>Nationality:</label><br><br>
        <select>
            <optgroup label="Asia">
                <option value="indonesian">Indonesian</option>
                <option value="singaporean">Singaporean</option>
                <option value="malaysian">Malaysian</option>
            </optgroup>
            <optgroup label="Europe">
                <option value="french">French</option>
                <option value="british">British</option>
            </optgroup>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox" name="language_spoken" value="Bahasa Indonesia">Bahasa Indonesia<br>
        <input type="checkbox" name="language_spoken" value="English">English<br>
        <input type="checkbox" name="language_spoken" value="Other">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea cols="30" rows="10"></textarea><br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
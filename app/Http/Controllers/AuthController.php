<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view('register');
    }

    public function kirim(Request $request){
        $nama_depan = $request['first_name'];
        $nama_belakang = $request['last_name'];
        $jk = $request['gender'];
        $bahasa = $request['language_spoken'];

        return view('welcome', compact('nama_depan', 'nama_belakang', 'jk', 'bahasa'));
    }
}
